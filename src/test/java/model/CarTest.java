package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    @Test
    void getBrand_ShouldReturnCorrectValue() {
        // Arrange
        Car car = new Car("TESLA", 2);

        // Act
        String returnValue = car.getBrand();

        // Assert
        assertEquals("TESLA", returnValue);
    }

    @Test
    void getDoors_ShouldReturnCorrectValue() {
        // Arrange
        Car car = new Car("TESLA", 2);

        // Act
        int returnValue = car.getDoors();

        // Assert
        assertEquals(2, returnValue);
    }

    @Test
    void setBrand_ShouldUpdateValue() {
        // Arrange
        Car car = new Car("TESLA", 2);

        // Act
        String newBrand = "MASERATI";
        car.setBrand(newBrand);

        // Assert
        assertEquals(newBrand, car.getBrand());
    }

    @Test
    void setDoors_ShouldUpdateValue() {
        // Arrange
        Car car = new Car("TESLA", 2);

        // Act
        int newDoorCount = 4;
        car.setDoors(newDoorCount);

        // Assert
        assertEquals(newDoorCount, car.getDoors());
    }
}