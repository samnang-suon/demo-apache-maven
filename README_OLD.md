# How To Create An Executable JAR
## Pre-requisites
* Java JDK
* JAVA_HOME environment variable

## Steps
1. Override maven-jar-plugin and maven-dependency-plugin with:

```xml
<build>
   <plugins>
      <plugin>
         <groupId>org.apache.maven.plugins</groupId>
         <artifactId>maven-jar-plugin</artifactId>
         <version>3.2.0</version>
         <configuration>
            <archive>
               <manifest>
                  <addClasspath>true</addClasspath>
                  <classpathPrefix>lib/</classpathPrefix>
                  <mainClass>App</mainClass>
               </manifest>
            </archive>
         </configuration>
      </plugin>

      <plugin>
         <groupId>org.apache.maven.plugins</groupId>
         <artifactId>maven-dependency-plugin</artifactId>
         <executions>
            <execution>
               <id>copy</id>
               <phase>package</phase>
               <goals>
                  <goal>copy-dependencies</goal>
               </goals>
               <configuration>
                  <outputDirectory>${project.build.directory}/lib</outputDirectory>
               </configuration>
            </execution>
         </executions>
      </plugin>
   </plugins>
</build>
```

2. Package your application:

         mvn clean package

![MvnCleanPackage](images/MvnCleanPackage.png "MvnCleanPackage")

3. Run your application:

SYNTAX:

      java -jar <AlsolutePathToYourJar>

EXAMPLE:

      java -jar .\target\DemoApacheMaven-1.0-SNAPSHOT.jar

![RunJavaJar](images/RunJavaJar.png "RunJavaJar")

## Error 'no main manifest attribute'
When trying to run with:

    java -jar .\target\DemoApacheMaven-1.0-SNAPSHOT.jar App

You will see:

    no main manifest attribute, in .\target\DemoApacheMaven-1.0-SNAPSHOT.jar

![NoMainManifest](images/NoMainManifest.png "NoMainManifest")

See: https://stackoverflow.com/questions/9689793/cant-execute-jar-file-no-main-manifest-attribute

## Error 'Exception in thread "main" java.lang.NoClassDefFoundError:'

See: https://stackoverflow.com/questions/5797860/maven-noclassdeffounderror-in-the-main-thread/13305236#13305236
