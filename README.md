# How To Create An Executable JAR
## Pre-requisites
* Java JDK
* JAVA_HOME environment variable

## Steps
1. Add the following code to your 'pom.xml':

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-assembly-plugin</artifactId>
            <executions>
                <execution>
                    <id>create-my-bundle</id>
                    <phase>package</phase>
                    <goals>
                        <goal>single</goal>
                    </goals>
                    <configuration>
                        <archive>
                            <manifest>
                                <mainClass>App</mainClass>
                            </manifest>
                        </archive>
                        <descriptorRefs>
                            <descriptorRef>jar-with-dependencies</descriptorRef>
                        </descriptorRefs>
                    </configuration>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

2. Package your application:

         mvn clean package

This will create 2 JAR files:

![PackageJarWithDependencies](images/PackageJarWithDependencies.png "PackageJarWithDependencies")

3. Run your application:

SYNTAX:

      java -jar <AlsolutePathToYourJar>

EXAMPLE:

      java -jar .\target\DemoApacheMaven-1.0-SNAPSHOT-jar-with-dependencies.jar

See the difference:

![StandaloneApp](images/StandaloneApp.png "StandaloneApp")

# ==================================================
# Maven Lifecycle vs Phase vs Plugin vs Goal
![MavenLifecycle01](images/MavenLifecycle01.png)

![MavenLifecycle02](images/MavenLifecycle02.png)

![MavenLifecycle03](images/MavenLifecycle03.png)

![MavenLifecycle04](images/MavenLifecycle04.png)

## Maven Lifecycle
1. default
2. clean
3. site

## Maven Phases
Each of these build lifecycles is defined by a different list of build phases, wherein a build phase represents a stage in the lifecycle.  
For example, the default lifecycle comprises of the following phases (for a complete list of the lifecycle phases, refer to the Lifecycle Reference):
* validate - validate the project is correct and all necessary information is available
* compile - compile the source code of the project
* test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
* package - take the compiled code and package it in its distributable format, such as a JAR.
* verify - run any checks on results of integration tests to ensure quality criteria are met
* install - install the package into the local repository, for use as a dependency in other projects locally
* deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.

## Maven Plugin:Goal
Example:
The **clean** and **package** arguments are build phases, while the **dependency:copy-dependencies** is a goal (of a plugin).

      mvn clean dependency:copy-dependencies package
            ^       ^              ^            ^
            |       |              |            |
          phase   plugin          goal        phase
Reference:
https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
